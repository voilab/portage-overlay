# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header $

EAPI=5
inherit eutils user

DESCRIPTION="Distributed key-value database management system"
HOMEPAGE="http://www.couchbase.com"
SRC_URI="http://packages.couchbase.com/releases/${PV}/${PN}_${PV}-debian7_amd64.deb"

SLOT="0"
KEYWORDS="~amd64"

RDEPEND=">=sys-libs/ncurses-5[tinfo]
		>=dev-libs/libevent-1.4.13
		>=dev-libs/cyrus-sasl-2
		>=media-video/rtmpdump-2.3
		>=dev-db/sqlite-3.5.9
		>=sys-libs/zlib-1.1.4
		virtual/jre"
DEPEND="${RDEPEND}"

PYTHON_COMPAT=( python2_7 )
PYTHON_REQ_USE='sqlite'
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

export CONFIG_PROTECT="${CONFIG_PROTECT} /opt/${PN}/var/lib/${PN}/"

S=${WORKDIR}

pkg_setup() {
	enewgroup couchbase
	enewuser couchbase -1 /bin/bash /opt/couchbase couchbase
}

src_unpack() {
	ar x "${DISTDIR}"/${A}
	cd ${WORKDIR}
	tar xzf data.tar.gz
}

src_prepare() {
	# basic cleanup
	rm -rf opt/couchbase/etc/{couchbase_init.d,couchbase_init.d.tmpl,init.d}
	find opt/couchbase/var/lib/couchbase/ -type f -delete || die

	sed -i -e "s#/opt/couchbase/var/lib/couchbase#/var/lib/couchbase#g" opt/couchbase/etc/couchbase/static_config

	epatch "${FILESDIR}"/cbtools-sqlite-version.patch
}

src_install() {
	dodir /var/lib/couchbase/
	cp -a opt/couchbase/var/lib/couchbase/* "${D}"/var/lib/couchbase/
	rm -rf opt/couchbase/var/
	dodir /var/lib/couchbase/{data,mnesia,tmp}
	fowners -R couchbase:couchbase /var/lib/couchbase/

	# bin install / copy
	dodir /opt/couchbase
	cp -a opt/couchbase/* "${D}"/opt/couchbase/

	dodir /var/log/couchbase/
	fowners -R couchbase:couchbase /var/log/couchbase/
	fowners -R couchbase:couchbase /opt/couchbase/

	newinitd "${FILESDIR}/couchbase-server.init" couchbase-server
	newconfd "${FILESDIR}/couchbase-server.conf" couchbase-server

	dosym /opt/couchbase/etc/logrotate.d/couchdb /etc/logrotate.d/couchbase

	insinto /etc/security/limits.d/
	doins "${FILESDIR}"/90-couchbase.conf
}
